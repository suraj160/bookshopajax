from django.urls import path
from . import views

app_name = 'app1'

urlpatterns = [

    path('', views.index, name = 'dfasdf'),
    path('saveBook/', views.saveBook, name = 'saveBook'),
    path('showBooks/', views.showBooks, name = 'showBooks'),
    path('deleteBooks/', views.deleteBooks, name = 'deleteBooks'),
    path('signup/', views.signUp, name = 'signUp'),#signUp not supported
    path('checkemail/', views.checkEmail, name = 'checkemail'),


]