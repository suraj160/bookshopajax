from django.shortcuts import render
from django.http import HttpResponse
from .models import Books, booksSerializer


# Create your views here.
def index(request):
	return render(request, 'index.html')

def saveBook(request):
	name = request.GET['name']
	price = request.GET['price']
	page = request.GET['page']
	book = Books(name = name, price = price, pages = page)
	try:
		book.save()
		return HttpResponse('true')
	except:
		return HttpResponse('false')


def showBooks(request):
	l = list()
	books = Books.objects.all().order_by('-id')
	for bk in books:
		ser = booksSerializer(bk)
		l.append(ser.data)
	import json;
	return HttpResponse(json.dumps(l))



def deleteBooks(request):
	try:
		print("***************")
		book = Books.objects.get(id = request.GET['id'])
		print(book)
		book.delete()
		return HttpResponse("true")
	except:
		return HttpResponse('false')


def signUp(request):
	print("**************************/*********")
	try:
		from .models import User
		user = User(name = request.GET['name'], email = request.GET['email'], password = request.GET['password'])
		user.save()
		return HttpResponse('true')
	except:
		return HttpResponse('false')


def checkEmail(request):
	from .models import User
	try:
		user = User.objects.get(email = request.GET['email'])
		return HttpResponse('true')
	except:
		return HttpResponse('false')





















