from django.db import models
from rest_framework import serializers

# Create your models here.

class Books(models.Model):
	name =  models.CharField(max_length =200 )
	price =  models.IntegerField()
	pages = models.IntegerField()

class booksSerializer(serializers.Serializer):
    name = serializers.CharField(max_length =200 )
    price = serializers.IntegerField()
    pages = serializers.IntegerField()
    id = serializers.IntegerField()


class User(models.Model):
	name  = models.CharField(max_length = 200)
	email = models.CharField(max_length = 200)
	password = models.CharField(max_length = 200)